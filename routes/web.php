<?php

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/clear', function () {
    $e = Artisan::call('cache:clear');
    $e1 = Artisan::call('config:cache');
    return redirect('/');
});

Route::get('/' , 'HomeController@index');
Route::get('/home' , 'HomeController@index');
Route::get('/about' ,'client\ClientController@about');
Route::get('/contact','client\ClientController@contact');
Route::post('/contact/send','client\ClientController@send');
Route::get('/photo','client\ClientController@photo');
Route::get('/video','client\ClientController@video');
Route::resource('/product', 'client\ProductController');
Route::get('/project/{id}', 'client\ClientController@project');
Route::get('/news/{id}', 'client\ClientController@news');
Route::resource('/category', 'client\CategoryController');
Route::group(['prefix' => '/administrator/grana'] ,function(){
    Auth::routes();
});


Route::group(['prefix' => 'admin' ,'middleware' => 'Administrator'], function () {

    Route::get('/', 'admin\AdminController@index');
    Route::get('/dashboard', 'admin\AdminController@index')->name('admin.dashboard');

    Route::DELETE('/Product/photo/{id}', 'admin\ProductController@destroyPhoto')->name('destroy.photo.product');
    Route::DELETE('/Project/photo/{id}', 'admin\ProjectController@destroyPhoto')->name('destroy.photo.project');

    Route::post('catalog/download', 'admin\CatalogController@getDownload');
    Route::get('catalog/upload', 'admin\CatalogController@upload')->name('catalog.upload');
    Route::post('catalog/upload/store', 'admin\CatalogController@store');
    Route::DELETE('catalog/destroy/{id}', 'admin\CatalogController@destroy');

    Route::get('photo/upload', 'admin\PhotoController@upload')->name('photo.upload');
    Route::post('photo/upload/store', 'admin\PhotoController@store');
    Route::DELETE('photo/destroy/{id}', 'admin\PhotoController@destroy');

    Route::post('video/download', 'admin\VideoController@getDownload');
    Route::get('video/upload', 'admin\VideoController@upload')->name('video.upload');
    Route::post('video/upload/store', 'admin\VideoController@store');
    Route::DELETE('video/destroy/{id}', 'admin\VideoController@destroy');

    Route::get('change/password', 'admin\AdminController@updateSecurityForm')->name('admin.passwordForm');
    Route::PATCH('{id}/change/password', 'admin\AdminController@updateSecurity')->name('admin.updateSecurity');

    Route::resource('/category', 'admin\CategoryController');
    Route::resource('/products', 'admin\ProductController');
    Route::resource('/about', 'admin\AboutController');
    Route::resource('/catalog', 'admin\CatalogController');
    Route::resource('/photo', 'admin\PhotoController');
    Route::resource('/video', 'admin\VideoController');
    Route::resource('/news', 'admin\NewsController');
    Route::resource('/projects', 'admin\ProjectController');
});


