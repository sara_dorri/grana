<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'رمزعبور باید حداقل 6 کاراکتر داشته باشد و با تکرار آن مطابقت داشته باشد.',
    'reset' => 'رمزعبور شما تغییر یافت !',
    'sent' => 'لینک بازیابی رمزعبور برای شما ارسال شد !',
    'token' => 'لینک بازیابی رمزعبور نامعتبر است !',
    'user' => "کاربری با این آدرس ایمیل یافت نشد .",

];
