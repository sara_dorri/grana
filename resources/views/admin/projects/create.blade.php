@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')

@stop

@section('head-content')
    <span class="fa fa-plus-circle myFont"></span> ایجاد پروژه جدید
@stop

@section('content')

    @if(Session::has('alert_error'))
        <div class="alert alert-danger">
            {{session('alert_error')}}
        </div>
    @endif

    @if(Session::has('alert_success'))
        <div class="alert alert-success">
            {{session('alert_success')}}
        </div>
    @endif
    {!! Form::open(['method'=>'POST', 'action'=>'admin\ProjectController@store' ,'files' => true , 'enctype'=> 'multipart/form-data' ,'id' => 'surveyForm']) !!}
    {{ csrf_field() }}
    
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'نام پروژه: ') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>


    <div class="form-group{{ $errors->has('photo.*') ? ' has-error' : '' }}">
        <label for="photo" class="form-label-customize">بارگذاری تصاویر پروژه( همه تصاویر را یکجا انتخاب کنید. )</label>
        <input type="file" id="photo" name="photo[]" multiple>
        @if ($errors->has('photo.*'))
            <span class="help-block">
                <strong>{{ $errors->first('photo.*') }}</strong>
            </span>
        @endif
    </div>


    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {!! Form::label('description', 'توضیحات پروژه: ') !!}
        {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>


    <div class="form-group">
        {!! Form::submit('ثبت', ['class'=>'form-control btn btn-info']) !!}
    </div>
    {!! Form::close() !!}



@endsection
