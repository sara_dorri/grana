@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')

@stop

@section('head-content')
    <span class="fa fa-edit myFont"></span> ویرایش پروژه
@stop

@section('content')

    @if(Session::has('alert_error'))
        <div class="alert alert-danger">
            {{session('alert_error')}}
        </div>
    @endif

    @if(sizeof($project->photos)>0)
        @foreach($project->photos as $photo)
            <img src="/image/projects/{{$photo->path}}" style="width:150px;">
            {!! Form::open(['method'=>'DELETE', 'action'=>['admin\ProjectController@destroyPhoto', $photo->id]]) !!}
            {!! Form::submit('حذف', ['class'=>'btn-xs', 'onclick' => "return confirm('آیا می خواهید این تصویر را حذف کنید؟')"]) !!}
            {!! Form::close() !!}
        @endforeach
    @endif

    {!! Form::model($project ,['method'=>'PATCH', 'action'=>['admin\ProjectController@update' ,$project->id] ,'files' => true ,'id' => 'surveyForm' ]) !!}
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('photo.*') ? ' has-error' : '' }}">
        <label for="photo" class="form-label-customize">بارگذاری تصاویر جدید پروژه(همه تصاویر را یکجا انتخاب کنید. )</label>
        <input type="file" id="photo" name="photo[]" multiple>
        @if ($errors->has('photo.*'))
            <span class="help-block">
                <strong>{{ $errors->first('photo.*') }}</strong>
            </span>
        @endif
    </div><!-- / . form-group -->


    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'نام پروژه: ') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    


    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {!! Form::label('description', 'توضیحات پروژه: ') !!}
        {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>


    <div class="form-group col-sm-6">
        {!! Form::submit('ویرایش', ['class'=>'form-control btn btn-info']) !!}
        {!! Form::close() !!}
    </div>

    {!! Form::open(['method'=>'DELETE', 'action'=>['admin\ProjectController@destroy', $project->id]]) !!}
    <div class="form-group col-sm-6">
        {!! Form::submit('حذف', ['class'=>'form-control btn btn-danger', 'onclick' => "return confirm('آیا می خواهید این پروژه را حذف کنید؟')"]) !!}
    </div>
    {!! Form::close() !!}

@endsection

