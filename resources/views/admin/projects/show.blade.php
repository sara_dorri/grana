@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')

@endsection

@section('head-content')
<span class="fa fa-info-circle myFont"></span> مشاهده اطلاعات پروژه
<span>{{ $project->name }}</span>

@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(Session::has('alert_error'))
                <div class="alert alert-danger">
                    {{session('alert_error')}}
                </div>
            @endif
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{$error}}</p>
                    @endforeach
                </div>
            @endif
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div style="overflow-x: auto;">
                                <table class="table table-striped table-bordered table-hover centerTable tdAlign"
                                       id="dataTables-example">
                                    <tr>
                                        <th class="text-center">نام پروژه</th>
                                        <td>{{$project->name}}</td>
                                    </tr>

                                    <tr>
                                        <th class="text-center">تصویر</th>
                                        <td>
                                            <div class="col-sm-12">
                                                @if(sizeof($project->photos)>0)
                                                    @foreach($project->photos as $key=>$value)
                                                        <img src="/image/projects/{{$value->path}}" style="width:250px;">
                                                    @endforeach
                                                @else
                                                    <img src="/image/no-photo.png" style="width:50px;">
                                                @endif
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th class="text-center">توضیحات</th>
                                        <td>{{$project->description}}</td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
