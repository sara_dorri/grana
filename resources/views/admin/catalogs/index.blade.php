@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')
@stop

@section('head-content')
    <span class="fa fa-download myFont"></span> کاتالوگ ها 
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if(Session::has('alert_error'))
                <div class="alert alert-danger">
                    {{session('alert_error')}}
                </div>
            @endif

            @if(isset($catalogs))
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-hover centerTable thAlign">
                            <thead>
                            <tr>
                                <th>--</th>
                                <th>نام فایل</th>
                                <th>پسوند فایل</th>
                                <th>زمان بارگزاری</th>
                                <th>توضیحات</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1 ?>
                            @foreach($catalogs as $catalog)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$catalog->name}}</td>
                                    <td>
                                        {{$catalog->mimeType}}
                                    </td>
                                    <td>{{jdate($catalog->created_at->diffForHumans())->ago()}}</td>
                                    <td>{{ str_limit($catalog->description,100 ,'...')}}</td>
                                    <td>

                                        <form role="form" method="POST" id="myForm" action="{{ url('admin/catalog/download') }}"
                                              enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="path" value="{{$catalog->path}}">
                                            <input type="hidden" name="mimeType" value="{{$catalog->mimeType}}">
                                            <button type="submit" class="btn btn-success btn-xs">دانلود</button>
                                        </form>
                                            {!! Form::open(['method'=>'DELETE', 'action'=>['admin\CatalogController@destroy', $catalog->id]]) !!}
                                            {!! Form::submit('حذف', ['class'=>'btn btn-danger btn-xs', 'onclick' => "return confirm('آیا می خواهید این فایل را حذف کنید؟')"]) !!}
                                            {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
