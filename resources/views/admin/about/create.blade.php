@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')

@stop

@section('head-content')
    <span class="fa fa-plus-circle myFont"></span> درباره ما
@stop

@section('content')

    @if(Session::has('alert_error'))
        <div class="alert alert-danger">
            {{session('alert_error')}}
        </div>
    @endif

    @if(Session::has('alert_success'))
        <div class="alert alert-success">
            {{session('alert_success')}}
        </div>
    @endif

    {!! Form::open(['method'=>'POST', 'action'=>'admin\AboutController@store' ,'files'=> true]) !!}

    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
        {!! Form::label('subject', 'عنوان:') !!}
        {!! Form::text('subject', null, ['class'=>'form-control']) !!}
        @if ($errors->has('subject'))
            <span class="help-block">
                <strong>{{ $errors->first('subject') }}</strong>
            </span>
        @endif
    </div>
    
    <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
        {!! Form::label('photo', 'تصویر:') !!}
        {!! Form::file('photo', null, ['class'=>'form-control']) !!}
        @if ($errors->has('photo'))
            <span class="help-block">
                 <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {!! Form::label('description', 'درباره ما: ') !!}
        {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>


    <div class="form-group">
        {!! Form::submit('ثبت', ['class'=>'form-control btn btn-info']) !!}
    </div>
    {!! Form::close() !!}

@endsection

@section('script')

@stop
