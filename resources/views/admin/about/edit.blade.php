@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')

@stop

@section('head-content')
    <span class="fa fa-edit myFont"></span> ویرایش درباره ما
@stop

@section('content')

    @if(Session::has('alert_error'))
        <div class="alert alert-danger">
            {{session('alert_error')}}
        </div>
    @endif
    {!! Form::model($about ,['method'=>'PATCH', 'action'=>['admin\AboutController@update' , $about->id ],'files'=> true]) !!}

    {{ csrf_field() }}

    @if($about->photo)
        <img src="/image/about/{{$about->photo}}" style="width:200px">
    @endif
    <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
        {!! Form::label('photo', 'تصویر:') !!}
        {!! Form::file('photo', null, ['class'=>'form-control']) !!}
        @if ($errors->has('photo'))
            <span class="help-block">
                 <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
        {!! Form::label('subject', 'عنوان:') !!}
        {!! Form::text('subject', null, ['class'=>'form-control']) !!}
        @if ($errors->has('subject'))
            <span class="help-block">
                <strong>{{ $errors->first('subject') }}</strong>
            </span>
        @endif
    </div>


    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {!! Form::label('description', 'درباره ما: ') !!}
        {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
    

    <div class="form-group col-sm-6">
        {!! Form::submit('ویرایش', ['class'=>'form-control btn btn-info']) !!}
        {!! Form::close() !!}
    </div>
    {!! Form::open(['method'=>'DELETE', 'action'=>['admin\AboutController@destroy', $about->id]]) !!}
    <div class="form-group col-sm-6">
        {!! Form::submit('حذف', ['class'=>'form-control btn btn-danger', 'onclick' => "return confirm('آیا می خواهید این مطلب را حذف کنید؟')"]) !!}
    </div>
    {!! Form::close() !!}



@endsection

@section('script')

@stop
