@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')
@stop

@section('head-content')
    <span class="myFont fa fa-list"></span>درباره ما
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(Session::has('alert_error'))
                <div class="alert alert-danger">
                    {{session('alert_error')}}
                </div>
                @endif
                @if(Session::has('alert_success'))
                    <div class="alert alert-success">
                        {{session('alert_success')}}
                    </div>
                    @endif
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{$error}}</p>
                            @endforeach
                    </div>
                    @endif

                @if($about != null)
                <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div style="overflow-x: auto;">
                                    <table class="table table-striped table-bordered table-hover centerTable tdAlign"
                                           id="dataTables-example">
                                        <tr>
                                            <th class="text-center">عنوان</th>
                                            <td>{{ $about->subject }}</td>
                                        </tr>
                                        <tr>
                                            <th class="text-center">تصویر</th>
                                            <td>
                                                @if($about->photo)
                                                    <img style="width:200px ;height:200px;" src="{{asset('/image/about/'. $about->photo)}}" />
                                                @endif
                                            </td>
                                        </tr>
                                        <tr class="text-center">
                                            <th class="text-center">توضیحات</th>
                                            <td>{{$about->description_fa}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                   <a href="{{ route('about.edit', $about->id) }}" class="btn btn-info">ویرایش</a>
                @endif
        </div>
    </div>
@endsection

@section('script')

@endsection