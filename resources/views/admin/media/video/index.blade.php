@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')
    <link href="{{asset('styles/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css">
@stop

@section('head-content')
    <span class="fa fa-download myFont"></span> ویدئو ها
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if(Session::has('alert_error'))
                <div class="alert alert-danger">
                    {{session('alert_error')}}
                </div>
            @endif

            @if(isset($videos))
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-hover centerTable thAlign">
                            <thead>
                            <tr>
                                <th>--</th>
                                <th>نام فایل</th>
                                <th>زمان بارگزاری</th>
                                <th>توضیحات</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1 ?>
                            @foreach($videos as $video)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$video->name}}</td>
                                    <td>{{jdate($video->created_at->diffForHumans())->ago()}}</td>
                                    <td>{{ str_limit($video->description,100 ,'...')}}</td>
                                    <td>
                                            {!! Form::open(['method'=>'DELETE', 'action'=>['admin\VideoController@destroy', $video->id]]) !!}
                                            {!! Form::submit('حذف', ['class'=>'btn btn-danger btn-xs', 'onclick' => "return confirm('آیا می خواهید این فایل را حذف کنید؟')"]) !!}
                                            {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('scripts/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('scripts/dataTables.bootstrap.min.js')}}"></script>

    {{--for data table--}}
    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>

@endsection
