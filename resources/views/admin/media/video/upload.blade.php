@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')
    <link href="{{asset('styles/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css">
@stop

@section('head-content')
    <span class="fa fa-upload myFont"></span>   بارگذاری ویدئو
@stop

@section('content')

    <div class="row">
        <div class="col-sm-12">
            @if(Session::has('alert_success'))
                <div class="alert alert-success">
                    {{session('alert_success')}}
                </div>
            @endif

            {!! Form::open(['method'=>'POST','id'=>'myForm', 'action'=>'admin\VideoController@store' , 'enctype'=> 'multipart/form-data']) !!}

                {{ csrf_field() }}


                <div class="form-group{{ $errors->has('path') ? ' has-error' : '' }}">
                    {!! Form::label('path', 'آدرس ویدئو : ') !!}
                    {!! Form::text('path', null, ['class'=>'form-control']) !!}
                    @if ($errors->has('path'))
                        <span class="help-block">
                        <strong>{{ $errors->first('path') }}</strong>
                    </span>
                    @endif
                </div>


                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {!! Form::label('name', 'نام ویدئو : ') !!}
                    {!! Form::text('name', null, ['class'=>'form-control']) !!}
                    @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>


                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    {!! Form::label('description', 'توضیحات :') !!}
                    {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
                    @if ($errors->has('description'))
                    <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                    @endif
                </div>
                

                <div class="form-group">
                    {!! Form::submit('بارگذاری', ['class'=>'form-control btn btn-info']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
