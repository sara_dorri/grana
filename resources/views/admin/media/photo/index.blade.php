@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')
@stop

@section('head-content')
    <span class="fa fa-download myFont"></span>گالری تصاویر
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">

            @if(Session::has('alert_error'))
                <div class="alert alert-danger">
                    {{session('alert_error')}}
                </div>
            @endif

            <div class="panel panel-default">
                <div class="panel-body">
                @if(isset($photos))
                    <div class="row">
                        @foreach($photos as $photo)
                            <div class="col-md-4">
                                <div class="thumbnail">
                                    <img src="/media/photo/{{$photo->path}}" style="height: 100px;">
                                    {!! Form::open(['method'=>'DELETE', 'action'=>['admin\PhotoController@destroy', $photo->id]]) !!}
                                    {!! Form::submit('حذف', ['class'=>'btn btn-danger btn-xs', 'onclick' => "return confirm('آیا می خواهید این تصویر را حذف کنید؟')"]) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        @endforeach
                            {{ $photos->links() }}
                    </div>
                @endif
                </div>
            </div>
        </div>
    </div>
@endsection
