@extends('layouts.admin_app')
@section('title', 'پنل مدیریت')

@section('head')

@stop

@section('head-content')
    <span class="fa fa-plus-circle myFont"></span> ایجاد دسته بندی جدید
@stop

@section('content')

    @if(Session::has('alert_error'))
        <div class="alert alert-danger">
            {{session('alert_error')}}
        </div>
    @endif

    @if(Session::has('alert_success'))
        <div class="alert alert-success">
            {{session('alert_success')}}
        </div>
    @endif

    {!! Form::open(['method'=>'POST', 'action'=>'admin\CategoryController@store' ,'files'=> true]) !!}

    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'نام دسته بندی: ') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>


    <div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
        {!! Form::label('parent_id', 'زیر شاخه: ') !!}
        {!! Form::select('parent_id',['' => '--انتخاب کنید--'] + $categories, null, ['class'=>'form-control']) !!}
        @if ($errors->has('parent_id'))
            <span class="help-block">
                <strong>{{ $errors->first('parent_id') }}</strong>
            </span>
        @endif
    </div>


    <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
        {!! Form::label('photo', 'تصویر:') !!}
        {!! Form::file('photo', null, ['class'=>'form-control']) !!}
        @if ($errors->has('photo'))
            <span class="help-block">
                 <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {!! Form::label('description', 'توضیحات: ') !!}
        {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>

   

    <div class="form-group">
        {!! Form::submit('ایجاد دسته بندی', ['class'=>'form-control btn btn-info']) !!}
    </div>
    {!! Form::close() !!}

@endsection

@section('script')

@stop
