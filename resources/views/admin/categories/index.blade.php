@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')
    <link href="{{asset('styles/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css">
@stop

@section('head-content')
    <span class="myFont fa fa-list"></span>  دسته بندی ها
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
                @if(Session::has('alert_error'))
                    <div class="alert alert-danger">
                        {{session('alert_error')}}
                    </div>
                @endif
                @if(Session::has('alert_success'))
                    <div class="alert alert-success">
                        {{session('alert_success')}}
                    </div>
                @endif
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{$error}}</p>
                        @endforeach
                    </div>
                @endif
            <div class="panel panel-default">
                <div class="dataTable_wrapper">
                    <div style="overflow-x: auto;">
                        <table class="table table-striped table-bordered table-hover centerTable thAlign"
                               id="dataTables-example">
                            <thead>
                            <tr>
                                <th>--</th>
                                <th>نام دسته بندی</th>
                                <th>تصویر</th>
                                <th>توضیحات</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            @if(isset($categories))
                                <tbody>
                                <?php $i = 1 ?>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$category->name}}</td>
                                        <td>
                                            @if($category->photo)
                                                <img style="width:70px ;height:50px;" src="/image/category/{{$category->photo}}" />
                                            @else
                                                <img style="width:70px ;height:50px;" src="/image/no-photo.png" />
                                            @endif
                                        </td>
                                        <td>
                                            {{str_limit($category->description ,100 ,'...')}}
                                        </td>
                                        <td>
                                           <a data-toggle="modal" data-target="#myModal{{$category->id}}" class="btn btn-warning btn-xs" href="#">ویرایش و حذف</a>
                                        </td>
                                    </tr>
                                    <!-- Modal -->
                                    <div id="myModal{{$category->id}}" class="modal fade" role="dialog">
                                        <div class="modal-dialog modal-sm">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title text-center">ویرایش دسته بندی </h4>
                                                </div>
                                                <div class="modal-body">
                                                    {!! Form::model($category ,['method'=>'PATCH', 'action'=>['admin\CategoryController@update' ,$category->id],'files'=> true ]) !!}
                                                    {{ csrf_field() }}
                                                    @if($category->photo)
                                                        <img src="/image/category/{{$category->photo}}" style="width:50px;">
                                                    @endif

                                                    <div class="form-group">
                                                        {!! Form::label('photo', 'تصویر:') !!}
                                                        {!! Form::file('photo',null, ['class'=>'form-control']) !!}
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::label('name', 'نام دسته بندی: ') !!}
                                                        {!! Form::text('name', null, ['class'=>'form-control']) !!}
                                                    </div>

                                                    <div class="form-group">
                                                        {!! Form::label('parent_id', 'زیر شاخه: ') !!}
                                                        {!! Form::select('parent_id',['' => '--انتخاب کنید--'] + $cat, null, ['class'=>'form-control']) !!}
                                                    </div>




                                                    <div class="form-group">
                                                        {!! Form::label('description', 'توضیحات: ') !!}
                                                        {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
                                                    </div>


                                                    <br>
                                                    <div class="form-group col-sm-6">
                                                        {!! Form::submit('ویرایش', ['class'=>'form-control btn btn-info']) !!}
                                                        {!! Form::close() !!}
                                                     </div>
                                                    {!! Form::open(['method'=>'DELETE', 'action'=>['admin\CategoryController@destroy', $category->id]]) !!}
                                                    <div class="form-group col-sm-6">
                                                        {!! Form::submit('حذف', ['class'=>'form-control btn btn-danger', 'onclick' => "return confirm('با حذف هر دسته تمام زیر دسته های آنها هم حذف میشوند، آیا اطمینان دارید؟')"]) !!}
                                                    </div>
                                                    {!! Form::close() !!}

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('scripts/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('scripts/dataTables.bootstrap.min.js')}}"></script>

    {{--for data table--}}
    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>

@endsection