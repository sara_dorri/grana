@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')
    <link href="{{asset('styles/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css">
@stop

@section('head-content')
    <span class="myFont fa fa-columns"></span>  لیست اخبار
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(Session::has('alert_error'))
                <div class="alert alert-danger">
                    {{session('alert_error')}}
                </div>
            @endif
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{$error}}</p>
                        @endforeach
                    </div>
                @endif
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div style="overflow-x: auto;">
                            <table class="table table-striped table-bordered table-hover centerTable thAlign"
                                   id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>--</th>
                                    <th>عنوان خبر</th>
                                    <th>تصویر</th>
                                    <th>متن خبر</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                                @if(isset($newss))
                                    <tbody>
                                    <?php $i = 1 ?>
                                    @foreach($newss as $news)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$news->name}} </td>
                                            <td>
                                                @if($news->photo)
                                                    <img src="/image/news/{{$news->photo}}" style="width:50px;">
                                                @else
                                                    <img src="/image/no-photo.png" style="width:50px;">
                                                @endif
                                            </td>
                                            <td>{{str_limit($news->description ,100 ,'...')}}</td>
                                            <td>
                                                <a class="btn btn-warning btn-xs" href="{{route('news.edit', $news->id)}}">ویرایش و حذف</a>
                                                <a class="btn btn-info btn-xs" href="{{route('news.show', $news->id)}}">مشاهده خبر</a>
                                            </td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('scripts/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('scripts/dataTables.bootstrap.min.js')}}"></script>

    {{--for data table--}}
    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>
@endsection