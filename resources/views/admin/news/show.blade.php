@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')

@endsection

@section('head-content')
<span class="fa fa-info-circle myFont"></span> مشاهده خبر 
<span>{{ $news->name }}</span>

@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(Session::has('alert_error'))
                <div class="alert alert-danger">
                    {{session('alert_error')}}
                </div>
            @endif
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{$error}}</p>
                    @endforeach
                </div>
            @endif
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div style="overflow-x: auto;">
                                <table class="table table-striped table-bordered table-hover centerTable tdAlign"
                                       id="dataTables-example">
                                    <tr>
                                        <th class="text-center">عنوان خبر</th>
                                        <td>{{$news->name}}</td>
                                    </tr>

                                    <tr>
                                        <th class="text-center">تصویر</th>
                                        <td>
                                            <div class="col-sm-12">
                                            @if($news->photo)
                                                <img style="width:200px ;height:200px;" src="{{asset('/image/news/'. $news->photo)}}" />
                                             @endif
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th class="text-center">توضیحات</th>
                                        <td>{{$news->description}}</td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
