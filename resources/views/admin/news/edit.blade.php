@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')

@stop

@section('head-content')
    <span class="fa fa-edit myFont"></span> ویرایش خبر
@stop

@section('content')

    @if(Session::has('alert_error'))
        <div class="alert alert-danger">
            {{session('alert_error')}}
        </div>
    @endif
    {!! Form::model($news ,['method'=>'PATCH', 'action'=>['admin\NewsController@update' ,$news->id] ,'files' => true ,'id' => 'surveyForm' ]) !!}
    {{ csrf_field() }}

    @if($news->photo)
        <img src="/image/news/{{$news->photo}}" style="width: 200px;">
    @endif
    <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
        {!! Form::label('photo', 'تصویر خبر: ') !!}
        {!! Form::file('photo',null, ['class'=>'form-control']) !!}
        @if ($errors->has('photo'))
            <span class="help-block">
                <strong>{{ $errors->first('photo') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'نام خبر: ') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {!! Form::label('description', 'توضیحات خبر: ') !!}
        {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>


    <div class="form-group col-sm-6">
        {!! Form::submit('ویرایش', ['class'=>'form-control btn btn-info']) !!}
        {!! Form::close() !!}
    </div>

    {!! Form::open(['method'=>'DELETE', 'action'=>['admin\NewsController@destroy', $news->id]]) !!}
    <div class="form-group col-sm-6">
        {!! Form::submit('حذف', ['class'=>'form-control btn btn-danger', 'onclick' => "return confirm('آیا می خواهید این خبر را حذف کنید؟')"]) !!}
    </div>
    {!! Form::close() !!}

@endsection

@section('script')

@stop
