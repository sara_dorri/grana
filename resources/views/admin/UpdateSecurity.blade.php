@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head-content')
    <span class="fa fa-key myFont"></span> تغییر رمز عبور
@stop

@section('content')

    @if(Session::has('alert_error'))
        <div class="alert alert-danger">
            {{session('alert_error')}}
        </div>
    @endif

    @if(Session::has('alert_success'))
        <div class="alert alert-success">
            {{session('alert_success')}}
        </div>
    @endif

    {!! Form::model( $user, ['method'=>'PATCH', 'action'=>['admin\AdminController@updateSecurity', $user->id]]) !!}
    <div class="form-group">
        {!! Form::label('username', 'نام کاربری ( غیر قابل تغییر ) : ') !!}
        {!! Form::text('username', $user->name, ['class'=>'form-control' ,'readonly' => 'true']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('oldPassword', 'رمز عبور قبلی: ') !!}
        {!! Form::password('oldPassword', ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('newPassword', 'رمز عبور جدید: ') !!}
        {!! Form::password('newPassword', ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('newPassword_confirmation', 'تکرار رمز جدید: ') !!}
        {!! Form::password('newPassword_confirmation', ['class'=>'form-control']) !!}
    </div>
    {!! Form::submit('ثبت تغییرات', ['class'=>'btn btn-info']) !!}
    {!! Form::close() !!}


@endsection

@section('script')

@stop
