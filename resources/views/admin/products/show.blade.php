@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')

@endsection

@section('head-content')
<span class="fa fa-info-circle myFont"></span> مشاهده اطلاعات محصول
<span>{{ $product->name }}</span>

@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(Session::has('alert_error'))
                <div class="alert alert-danger">
                    {{session('alert_error')}}
                </div>
            @endif
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <p>{{$error}}</p>
                    @endforeach
                </div>
            @endif
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div style="overflow-x: auto;">
                                <table class="table table-striped table-bordered table-hover centerTable tdAlign"
                                       id="dataTables-example">
                                    <tr>
                                        <th class="text-center">نام محصول</th>
                                        <td>{{$product->name}}</td>
                                    </tr>

                                    <tr>
                                        <th class="text-center">دسته بندی</th>
                                        <td>{{$product->category->name}}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-center">تصویر</th>
                                        <td>
                                            <div class="col-sm-12">
                                                @if(sizeof($product->photos)>0)
                                                    @foreach($product->photos as $key=>$value)
                                                        <img src="/image/products/{{$value->path}}" style="width:250px;">
                                                    @endforeach
                                                @else
                                                    <img src="/image/no-photo.png" style="width:50px;">
                                                @endif
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th class="text-center">توضیحات</th>
                                        <td>{{$product->description}}</td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection

@section('script')


@endsection