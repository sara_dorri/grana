@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')
    <link href="{{asset('styles/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css">
@stop

@section('head-content')
    <span class="myFont fa fa-columns"></span>  لیست محصولات
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(Session::has('alert_error'))
                <div class="alert alert-danger">
                    {{session('alert_error')}}
                </div>
            @endif
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{$error}}</p>
                        @endforeach
                    </div>
                @endif
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div style="overflow-x: auto;">
                            <table class="table table-striped table-bordered table-hover centerTable thAlign"
                                   id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>--</th>
                                    <th>نام محصول</th>
                                    <th>تصویر</th>
                                    <th>دسته بندی</th>
                                    <th>توضیحات</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                                @if(isset($products))
                                    <tbody>
                                    <?php $i = 1 ?>
                                    @foreach($products as $product)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$product->name}} </td>
                                            <td>
                                                @if(sizeof($product->photos)>0)
                                                    @php $i=1; @endphp
                                                    @foreach($product->photos as $key=>$value)
                                                        @if($i==1)
                                                            <img src="/image/products/{{$value->path}}" style="width:50px;">
                                                        @endif
                                                        @php $i++; @endphp
                                                    @endforeach
                                                @else
                                                    <img src="/image/no-photo.png" style="width:50px;">
                                                @endif
                                            </td>
                                            <td>{{ $product->category->name }} </td>
                                            <td>{{str_limit($product->description ,100 ,'...')}}</td>
                                            <td>
                                                <a class="btn btn-warning btn-xs" href="{{route('products.edit', $product->id)}}">ویرایش و حذف</a>
                                                <a class="btn btn-info btn-xs" href="{{route('products.show', $product->id)}}">اطلاعات محصول</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('scripts/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('scripts/dataTables.bootstrap.min.js')}}"></script>

    {{--for data table--}}
    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>
@endsection