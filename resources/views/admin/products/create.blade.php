@extends('layouts.admin_app')
@section('title', 'پنل مدیریتی')

@section('head')

@stop

@section('head-content')
    <span class="fa fa-plus-circle myFont"></span> ایجاد محصول جدید
@stop

@section('content')

    @if(Session::has('alert_error'))
        <div class="alert alert-danger">
            {{session('alert_error')}}
        </div>
    @endif

    @if(Session::has('alert_success'))
        <div class="alert alert-success">
            {{session('alert_success')}}
        </div>
    @endif
    {!! Form::open(['method'=>'POST', 'action'=>'admin\ProductController@store' ,'files' => true , 'enctype'=> 'multipart/form-data' ,'id' => 'surveyForm']) !!}
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'نام محصول: ') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    

    <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
        {!! Form::label('category_id', 'دسته بندی: ') !!}
        {!! Form::select('category_id', ['' => '--انتخاب کنید--'] + $categories ,null, ['class'=>'form-control']) !!}
        @if ($errors->has('category_id'))
            <span class="help-block">
                <strong>{{ $errors->first('category_id') }}</strong>
            </span>
        @endif
    </div>


    <div class="form-group{{ $errors->has('photo.*') ? ' has-error' : '' }}">
        <label for="photo" class="form-label-customize">بارگذاری تصاویر محصول( همه تصاویر را یکجا انتخاب کنید. )</label>
        <input type="file" id="photo" name="photo[]" multiple>
        @if ($errors->has('photo.*'))
            <span class="help-block">
                <strong>{{ $errors->first('photo.*') }}</strong>
            </span>
        @endif
    </div><!-- / . form-group -->

    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
        {!! Form::label('description', 'توضیحات محصول: ') !!}
        {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>


    <div class="form-group">
        {!! Form::submit('ثبت', ['class'=>'form-control btn btn-info']) !!}
    </div>
    {!! Form::close() !!}



@endsection

@section('script')

@stop
