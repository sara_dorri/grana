<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <link href="{{asset('styles/bootstrap.min.css')}}" rel="stylesheet">
    {{--<link href="{{asset('styles/new-style.css')}}" rel="stylesheet">--}}
    <link href="{{asset('styles/sb-admin-2.css')}}" rel="stylesheet">
    <link href="{{asset('styles/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('styles/jquery.Bootstrap-PersianDateTimePicker.css')}}" rel="stylesheet" type="text/css">
    <script src="{{asset('scripts/jalaali.js')}}"></script>
    <script src="{{asset('scripts/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('scripts/bootstrap.min.js')}}"></script>
    <script src="{{asset('scripts/jquery.Bootstrap-PersianDateTimePicker.js')}}"></script>
    <script src="{{asset('scripts/sb-admin-2.js')}}"></script>



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield('head')
</head>

<body>
<div id="wrapper">
    <nav class="navbar bg-primary navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header pull-left">
            {{--<a class="navbar-brand" href="{{ route('admin.dashboard') }}">{{ \Illuminate\Support\Facades\Auth::user()->name }}</a>--}}
        </div>
        <div class="navbar-header pull-left">
            <span>{{  ' امروز: ' . jDate::forge('today')->format('%d %B %Y')  }}</span>
</div>
        <div class="navbar-header pull-right">
            <a class="navbar-brand" href="/" target="_blank">مشاهده وب سایت</a>
        </div>
    </nav>
    <div id="page-wrapper">
      <div class="col-sm-3 col-sm-push-9" style="padding: 0 !important;">
        <div class="navbar-in sidebar" role="navigation" style="width: 100%;">
            <div class="sidebar-nav navbar-collapse collapse">
                <ul class="nav" id="side-menu">

                        <li data-toggle="collapse" data-target="#setting" class="collapsed remain">
                            <a href="#"><i class="fa fa-cogs"></i>  تنظیمات <span class="fa fa-caret-left pull-left"></span></a>
                        </li>

                        <ul class="nav nav-second-level collapse" id="setting">
                            <li>
                                <a href="{{ route('admin.passwordForm') }}"><span class="fa fa-key" >&nbsp;</span>تغییر رمز عبور</a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <span class="fa fa-sign-out" >&nbsp;</span>خروج
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    <li data-toggle="collapse" data-target="#category" class="collapsed remain">
                        <a href="#"><i class="fa fa-tasks"></i>  دسته بندی محصولات <span class="fa fa-caret-left pull-left"></span></a>
                    </li>

                    <ul class="nav nav-second-level collapse" id="category">
                        <li>
                            <a href="{{ route('category.create') }}">اضافه کردن دسته بندی جدید</a>
                        </li>
                        <li>
                            <a href="{{ route('category.index') }}">نمایش و ویرایش دسته بندی های موجود </a>
                        </li>
                    </ul>

                    <li data-toggle="collapse" data-target="#project" class="collapsed remain">
                        <a href="#"><i class="fa fa-cubes"></i>  پروژه ها <span class="fa fa-caret-left pull-left"></span></a>
                    </li>

                    <ul class="nav nav-second-level collapse" id="project">
                        <li>
                            <a href="{{ route('projects.create') }}">اضافه کردن پروژه جدید</a>
                        </li>
                        <li>
                            <a href="{{ route('projects.index') }}">نمایش و ویرایش پروژه های موجود </a>
                        </li>
                    </ul>


                    <li data-toggle="collapse" data-target="#news" class="collapsed remain">
                        <a href="#"><i class="fa fa-newspaper-o"></i> اخبار  <span class="fa fa-caret-left pull-left"></span></a>
                    </li>

                    <ul class="nav nav-second-level collapse" id="news">
                        <li>
                            <a href="{{ route('news.create') }}">اضافه کردن اخبار</a>
                        </li>
                        <li>
                            <a href="{{ route('news.index') }}">نمایش و ویرایش اخبار </a>
                        </li>
                    </ul>

                    <li data-toggle="collapse" data-target="#products" class="collapsed remain">
                        <a href="#"><i class="fa fa-barcode"></i> محصولات  <span class="fa fa-caret-left pull-left"></span></a>
                    </li>

                    <ul class="nav nav-second-level collapse" id="products">
                        <li>
                            <a href="{{ route('products.create') }}">اضافه کردن محصول جدید</a>
                        </li>
                        <li>
                            <a href="{{ route('products.index') }}">نمایش و ویرایش محصولات موجود </a>
                        </li>
                    </ul>


                    <li data-toggle="collapse" data-target="#catalog" class="collapsed remain">
                        <a href="#"><i class="fa fa-file"></i> کاتالوگ <span class="fa fa-caret-left pull-left"></span></a>
                    </li>

                    <ul class="nav nav-second-level collapse" id="catalog">

                        <li>
                            <a href="{{ route('catalog.index') }}">نمایش کاتالوگ ها </a>
                        </li>

                        <li>
                            <a href="{{ route('catalog.upload') }}">بارگذاری کاتالوگ </a>
                        </li>
                    </ul>

                    <li data-toggle="collapse" data-target="#photo" class="collapsed remain">
                        <a href="#"><i class="fa fa-photo"></i> گالری تصاویر <span class="fa fa-caret-left pull-left"></span></a>
                    </li>

                    <ul class="nav nav-second-level collapse" id="photo">

                        <li>
                            <a href="{{ route('photo.index') }}">نمایش تصاویر </a>
                        </li>

                        <li>
                            <a href="{{ route('photo.upload') }}">بارگذاری تصویر </a>
                        </li>
                    </ul>

                    <li data-toggle="collapse" data-target="#video" class="collapsed remain">
                        <a href="#"><i class="fa fa-video-camera"></i> گالری ویدئو <span class="fa fa-caret-left pull-left"></span></a>
                    </li>

                    <ul class="nav nav-second-level collapse" id="video">

                        <li>
                            <a href="{{ route('video.index') }}">نمایش ویدئوها </a>
                        </li>

                        <li>
                            <a href="{{ route('video.upload') }}">بارگذاری ویدئو </a>
                        </li>
                    </ul>

                    <li data-toggle="collapse" data-target="#about" class="collapsed remain">
                        <a href="#"><i class="fa fa-info-circle"></i> درباره ما <span class="fa fa-caret-left pull-left"></span></a>
                    </li>

                    <ul class="nav nav-second-level collapse" id="about">
                        <li>
                            <a href="{{ route('about.create') }}">ایجاد </a>
                        </li>
                        <li>
                            <a href="{{ route('about.index') }}">مشاهده و ویرایش </a>
                        </li>
                    </ul>

                </ul>
            </div>
        </div>
    </div>

    <div class="col-sm-9 col-sm-pull-3" style="padding: 0 0 0 15px !important;">
        <div id="page-wrapper" style="border-radius: 14px;">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="page-header">@yield('head-content')</h2>
                </div>
            </div>
            <div class="col-sm-12">
                @yield('content')
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</div>
<script src="{{asset('scripts/jquery-2.2.4.min.js')}}"></script>
@yield('script')

</body>
</html>
