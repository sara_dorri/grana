<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default" style="margin-bottom: 0">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">گرانا بتن</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="/">صفحه اصلی</a></li>
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">محصولات<i class="caret icon-align-right"></i></a>
                    <ul class="dropdown-menu multi-level">
                        @if(isset($roots))
                            @foreach($roots as $key=>$value)
                                @if(sizeof($value->getImmediateDescendants())== 0)
                                    <li><a href="{{url('/category', $value->id)}}">{{$value->name}}</a></li>
                                @else
                                <li class="dropdown-submenu">
                                    <a href="{{url('/category', $value->id)}}" class="dropdown-toggle" data-toggle="dropdown">{{$value->name}}<i class="caret"></i></a>
                                    <ul class="dropdown-menu">
                                        @foreach($value->getImmediateDescendants() as $child)
                                            @if(sizeof($child->getImmediateDescendants())== 0)
                                                <li><a href="{{url('/category', $child->id)}}">{{ $child->name }}</a></li>
                                            @else
                                            <li class="dropdown-submenu">
                                                <a href="{{url('/category', $child->id)}}" class="dropdown-toggle" data-toggle="dropdown">{{ $child->name }}<i class="caret"></i></a>
                                                <ul class="dropdown-menu">
                                                    @foreach($child->getImmediateDescendants() as $children)
                                                        @if(sizeof($children->getImmediateDescendants())== 0)
                                                            <li><a href="{{url('/category', $children->id)}}">{{ $children->name }}</a></li>
                                                        @else
                                                            <li class="dropdown-submenu">
                                                                <a href="{{url('/category', $children->id)}}" class="dropdown-toggle" data-toggle="dropdown">{{ $children->name }}<i class="caret"></i></a>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="{{url('/category', $children->id)}}">{{ $children->name }}</a></li>
                                                                </ul>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                                @endif
                            @endforeach
                        @endif
                    </ul><!-- /.dropdown-menu -->
                </li>
                <li><a href="#">بازرگانی</a></li>
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">چندرسانه ای<i class="caret icon-align-right"></i></a>
                    <ul class="dropdown-menu multi-level">
                        <li><a href="{{ url('/photo') }}">گالری تصاویر</a></li>
                        <li><a href="{{ url('/video') }}">گالری فیلم</a></li>
                    </ul><!-- /.dropdown-menu -->
                </li>
                <li><a href="{{ url('/about') }}">درباره ما</a></li>
                <li><a href="{{ url('/contact') }}">تماس با ما</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

@yield('content')


<footer id="footer">
    <div class="container">
        <div class="row mb1" style="">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="footer-box">
                    <h5 class="footer-box-head">درباره گرانا بتن</h5>
                    <p class="text-justify">
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها
                        و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و
                        کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                    </p>
                </div><!-- /.col-x-x -->
            </div><!-- /col-x-x -->
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="footer-box">
                    <h5 class="footer-box-head">آخرین اخبار</h5>
                    <ul class="list-unstyled footer-box-list-note">
                        @if(isset($news))
                            @foreach($news as $key=>$value)
                                <li><a href="{{url('/news' ,$value->id)}}">{{$value->name}}</a></li>
                            @endforeach
                        @endif
                    </ul>
                </div><!-- /.col-x-x -->
            </div><!-- /col-x-x -->
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="footer-box">
                    <h5 class="footer-box-head">راه های تماس با ما</h5>
                    <ul class="list-unstyled footer-box-list-note">
                        <li><span class="fa fa-phone text-orange icon-align-left"></span>086-00000</li>
                        <li><span class="fa fa-envelope-o text-orange icon-align-left"></span>info@gmail.com</li>
                        <li><span class="fa fa-clock-o text-orange icon-align-left"></span>شنبه تا پنج شنبه</li>
                    </ul>
                </div><!-- /.col-x-x -->
            </div><!-- /col-x-x -->
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="footer-box">
                    <h5 class="footer-box-head">گرانا بتن</h5>
                    <img class="center-block img-responsive" style="max-width: 140px" src="/img/logo.jpg" alt="image">
                </div><!-- /.col-x-x -->
            </div><!-- /col-x-x -->
        </div><!-- /.Row -->
        <div class="row">
            <div style="border-top: 1px solid white;margin-top: 1em;padding-top: 0.5em">
                <div class="col-xs-12 col-sm-7">تمامی حقوق مادی و معنوی سایت برای <a href="/">گرانا بتن</a> محفوظ میباشد .</div>
                <ul class="list-inline col-xs-12 col-sm-5 footer-social">
                    <li class="footer-social-list"><a href="#"><span class="fa fa-android"></span></a></li>
                    <li class="footer-social-list"><a href="#"><span class="fa fa-send"></span></a></li>
                    <li class="footer-social-list"><a href="#"><span class="fa fa-instagram"></span></a></li>
                    <li class="footer-social-list"><a href="#"><span class="fa fa-google-plus"></span></a></li>
                </ul>
            </div>
        </div><!-- /.Row -->
    </div><!-- /.container -->
</footer><!-- /#footer -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/custom.js"></script>
@yield('script')
</body>
</html>
