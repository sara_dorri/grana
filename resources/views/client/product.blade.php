@extends('layouts.theme')
@section('title', 'گرانا بتن | محصولات')

@section('content')
    <section id="projects-page">
        <div class="container-fluid">
            <div class="row">
                <div class="sp-page-title">
                    <div class="container">
                        <h3 class="sp-page-title-text">{{$product->name}}</h3>
                    </div><!-- /.container -->
                </div><!-- /.sp-page-title -->
            </div><!-- /.Row -->
        </div><!-- /.container-fluid -->
        <div class="container" style="min-height: 400px;padding-top: 1em">
            <div class="row">
                <div class="col-xs-12 col-sm-9">
                    <div id="carousel" class="carousel carousel-product slide" data-ride="carousel">

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="/image/products/{{$product->photo}}" alt="...">
                            </div><!-- /.item -->
                        </div><!-- /.carousel-inner -->

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                            <span class="fa fa-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                            <span class="fa fa-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div><!-- /#carousel -->
                    <br>
                    <div class="products-page-title">
                        <h3 class="text-head" style="color: #333">{{$product->name}}</h3>
                        <div class="text-head-line mb1"></div>
                        <p class="text-justify">
                            {{$product->description}}
                        </p>
                    </div>
                </div><!-- /.col-x-x -->
                <div class="col-xs-12 col-sm-3">
                    <h4>آخرین محصولات</h4>
                    <div class="list-group list-group-products">
                        @if(isset($lastProducts))
                            @foreach($lastProducts as $p)
                                <a href="#" class="list-group-item">{{$p->name}}</a>
                            @endforeach
                        @endif
                    </div><!-- /.list-group -->
                    <div class="jumbotron jumbotron-products " style="padding: 15px">
                        <h4 class="jumbotron-products-head">درباره شرکت</h4>
                        <div class="jumbotron-products-desc"><span class="fa fa-phone text-orange icon-align-left"></span>086-00000</div>
                        <div class="jumbotron-products-desc"><span class="fa fa-envelope-o text-orange icon-align-left"></span>info@gmail.com</div>
                        <div class="jumbotron-products-desc"><span class="fa fa-clock-o text-orange icon-align-left"></span>شنبه تا پنج شنبه</div>
                    </div><!-- /.jumbotron -->
                </div><!-- /.col-x-x -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
@endsection