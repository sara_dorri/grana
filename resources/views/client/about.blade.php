@extends('layouts.theme')
@section('title', 'گرانا بتن | درباره ما')

@section('content')

    <section id="contact-page">
        <div class="container-fluid">
            <div class="row">
                <div class="sp-page-title">
                    <div class="container">
                        <h3 class="sp-page-title-text">درباره ما</h3>
                    </div><!-- /.container -->
                </div><!-- /.sp-page-title -->
            </div><!-- /.Row -->
        </div><!-- /.container-fluid -->
        <div class="container" style="min-height: 400px;padding-top: 1em">
            <div class="about-page-title col-xs-12 col-sm-7">
                <h3 class="text-head" style="color: #333">{{ $about ? $about->subject : '' }}</h3>
                <div class="text-head-line"></div>
                <p class="text-justify">{{$about ? $about->description : ''}}</p>
            </div><!-- /.col-x-x -->
            <div class="col-xs-12 col-sm-5">
                <img class="img-responsive center-block" src="/image/about/{{$about ? $about->photo : ''}}" alt="image">
            </div><!-- /.col-x-x -->
        </div><!-- /.container -->
    </section>


@endsection
