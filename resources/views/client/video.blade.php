@extends('layouts.theme')
@section('title', 'گرانا بتن | گالری فیلم')

@section('content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="sp-page-title">
                    <div class="container">
                        <h3 class="sp-page-title-text">گالری فیلم</h3>
                    </div><!-- /.container -->
                </div><!-- /.sp-page-title -->
            </div><!-- /.Row -->
        </div><!-- /.container-fluid -->

        <div class="container" style="min-height: 300px;padding-top: 1em;padding-bottom: 1em">
            <div class="row">

                @if(isset($videos))
                    @foreach($videos as $video)
                        <div class="col-xs-12 col-md-6">
                            <!-- 16:9 aspect ratio -->
                            {{$video->path}}
                        </div><!-- /col-x-x -->
                    @endforeach
                @endif

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

@endsection