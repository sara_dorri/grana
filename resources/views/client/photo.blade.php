@extends('layouts.theme')
@section('title', 'گرانا بتن | گالری تصاویر')

@section('content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="sp-page-title">
                    <div class="container">
                        <h3 class="sp-page-title-text">گالری تصاویر</h3>
                    </div><!-- /.container -->
                </div><!-- /.sp-page-title -->
            </div><!-- /.Row -->
        </div><!-- /.container-fluid -->

        <div class="container" style="min-height: 400px;padding-top: 1em">
            <div class="row">
                @if(isset($photo))
                    @foreach($photo as $key=>$value)
                        <div class="col-xs-6 col-md-3 thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-caption="{{$value->name}}" data-image="/media/photo/{{$value->path}}" data-target="#image-gallery">
                                <img class="img-responsive" src="/media/photo/{{$value->path}}" alt="Short alt text">
                            </a>
                        </div><!-- /col-x-x -->
                    @endforeach
                @endif
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>

    <!-- Modal -->
    <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span class="sr-only">بستن</span>
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="image-gallery-title"></h4>
                </div>
                <div class="modal-body">
                    <img id="image-gallery-image" class="img-responsive center-block" src="">
                </div>
                <div class="modal-footer">

                    <div class="col-md-2">
                        <button type="button" class="btn btn-primary" id="show-previous-image"><span class="fa fa-arrow-right"></span></button>
                    </div>

                    <div class="col-md-8 text-justify" id="image-gallery-caption">

                    </div>

                    <div class="col-md-2">
                        <button type="button" id="show-next-image" class="btn btn-default"><span class="fa fa-arrow-left"></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Modal -->

@endsection

@section('script')
    <script>
        $(document).ready(function(){

            loadGallery(true, 'a.thumbnail');

            //This function disables buttons when needed
            function disableButtons(counter_max, counter_current){
                $('#show-previous-image, #show-next-image').show();
                if(counter_max == counter_current){
                    $('#show-next-image').hide();
                } else if (counter_current == 1){
                    $('#show-previous-image').hide();
                }
            }

            /**
             *
             * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
             * @param setClickAttr  Sets the attribute for the click handler.
             */

            function loadGallery(setIDs, setClickAttr){
                var current_image,
                        selector,
                        counter = 0;

                $('#show-next-image, #show-previous-image').click(function(){
                    if($(this).attr('id') == 'show-previous-image'){
                        current_image--;
                    } else {
                        current_image++;
                    }

                    selector = $('[data-image-id="' + current_image + '"]');
                    updateGallery(selector);
                });

                function updateGallery(selector) {
                    var $sel = selector;
                    current_image = $sel.data('image-id');
                    $('#image-gallery-caption').text($sel.data('caption'));
                    $('#image-gallery-title').text($sel.data('title'));
                    $('#image-gallery-image').attr('src', $sel.data('image'));
                    disableButtons(counter, $sel.data('image-id'));
                }

                if(setIDs == true){
                    $('[data-image-id]').each(function(){
                        counter++;
                        $(this).attr('data-image-id',counter);
                    });
                }
                $(setClickAttr).on('click',function(){
                    updateGallery($(this));
                });
            }
        });
    </script>
@endsection