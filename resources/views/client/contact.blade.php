@extends('layouts.theme')
@section('title', 'گرانا بتن | تماس با ما')

@section('content')
    <section id="contact-page">
        <div class="container-fluid">
            <div class="row">
                <div class="sp-page-title">
                    <div class="container">
                        <h3 class="sp-page-title-text">تماس با ما</h3>
                    </div><!-- /.container -->
                </div><!-- /.sp-page-title -->
            </div><!-- /.Row -->
            <div class="row">
                <img class="img-responsive center-block" style="width: 100%" src="/img/map.jpg" alt="map">
            </div><!-- /.Row -->
        </div><!-- /.container-fluid -->
        <div class="container">
            <div class="row">
                <div class="contact-page-head text-center">
                    <h3 class="contact-page-title">تماس با ما</h3>
                    <p class="contact-page-desc">لورم ایپسون متن ساختگی است که طراحان گرافیست و عکاسان از آن برای کارهای خود استفاده میکنند .</p>
                </div>
            </div><!-- /.Row -->
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="row mb1">
                        <div class="c-icon pull-right"><span class="fa fa-map-o"></span></div>
                        <div class="c-desc">
                            <h5 class="c-desc-head">آدرس ما</h5>
                            <div class="c-desc-t">ایران / تهران /...</div>
                        </div>
                    </div><!-- /item -->
                    <div class="row mb1">
                        <div class="c-icon pull-right"><span class="fa fa-envelope-o"></span></div>
                        <div class="c-desc">
                            <h5 class="c-desc-head">ایمیل ما</h5>
                            <div class="c-desc-t">info@gmail.com</div>
                        </div>
                    </div><!-- /item -->
                    <div class="row mb1">
                        <div class="c-icon pull-right"><span class="fa fa-phone"></span></div>
                        <div class="c-desc">
                            <h5 class="c-desc-head">تماس با ما</h5>
                            <div class="c-desc-t">021-000000</div>
                        </div>
                    </div><!-- /item -->
                </div><!-- /.col-x-x -->
                <div class="col-xs-12 col-sm-8">
                    <form action="{{url('/contact/send')}}" method="post" data-toggle="validator">
                        {{ csrf_field() }}
                        <!-- Text input-->
                        <div class="form-group has-feedback">
                            <input id="name" name="name" type="text" autocomplete="off" placeholder="نام و نام خانوادگی" class="form-control input-md" data-minlength="3" maxlength="30" data-error="نام و نام خانوادگی خود را وارد کنید." required>
                            <div class="col-xs-12 help-block with-errors"></div>
                        </div><!-- / Text input-->
                        <!-- Number input-->
                        <div class="form-group has-feedback">
                            <input id="email" name="email" type="email" autocomplete="off" placeholder="آدرس ایمیل" class="form-control input-md" data-error="آدرس ایمیل خود را وارد کنید." required>
                            <div class="help-block with-errors"></div>
                        </div><!-- / Number input-->
                        <div class="form-group has-feedback">
                            <textarea name="description" class="form-control" placeholder="متن پیام" rows="5" data-minlength="10" maxlength="1000" pattern="^[_a-z]{1,}$" data-error="متن پیام خود را وارد کنید." required></textarea>
                            <div class="col-xs-12 help-block with-errors"></div>
                        </div>
                        <div class="form-group has-feedback text-right">
                            <button type="submit" class="btn btn-info">ارسال</button>
                        </div>
                    </form>
                </div><!-- /.col-x-x -->
            </div><!-- /.Row -->
        </div><!-- /.container -->
    </section>
@endsection

@section('script')
    <script src="js/validator.min.js" defer="defer"></script>
@endsection