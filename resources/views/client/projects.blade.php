@extends('layouts.theme')
@section('title', 'گرانا بتن | پروژه ها')

@section('content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="sp-page-title">
                    <div class="container">
                        <h3 class="sp-page-title-text">پروژه ها</h3>
                    </div><!-- /.container -->
                </div><!-- /.sp-page-title -->
            </div><!-- /.Row -->
        </div><!-- /.container-fluid -->
        <div class="container">
            <div class="row">
                <div class="controls filter-controls-custom text-center">
                    <button type="button" class="btn btn-default control" data-filter="all">همه پروژه ها</button>
                    <button type="button" class="btn btn-default control" data-filter=".green">پروژه یک</button>
                    <button type="button" class="btn btn-default control" data-filter=".blue">پروژه دو</button>
                </div>
                <div class="main-content col-xs-12" style="min-height: 300px">
                    <div class="row">
                        <div class="mix col-xs-6 col-sm-4 green">
                            <div class="projects-box projects-box-portfolio">
                                <img class="img-responsive center-block" src="src/img/project-1.jpg" alt="image">
                                <div class="overlay overlay-portfolio">
                                    <div class="overlay-text">
                                        <a href="#" class="btn btn-theme">بیشتر بدانید</a>
                                        <a href="#" class="btn btn-theme">بیشتر بدانید</a>
                                    </div>
                                </div><!-- /.overlay -->
                            </div><!-- /.projects-box -->
                        </div><!-- /.mix -->
                        <div class="mix col-xs-6 col-sm-4 blue">
                            <div class="projects-box projects-box-portfolio">
                                <img class="img-responsive center-block" src="src/img/project-1.jpg" alt="image">
                                <div class="overlay overlay-portfolio">
                                    <div class="overlay-text">
                                        <a href="#" class="btn btn-theme">بیشتر بدانید</a>
                                        <a href="#" class="btn btn-theme">بیشتر بدانید</a>
                                    </div>
                                </div><!-- /.overlay -->
                            </div><!-- /.projects-box -->
                        </div><!-- /.mix -->

                    </div><!-- /.row ... -->
                </div><!-- /.main-content -->

            </div><!-- /.Row -->
        </div><!-- /.container -->
    </section><!-- /.container-fluid -->
@endsection

@section('script')
    <script type='text/javascript' src='src/js/mA.js'></script>
@endsection