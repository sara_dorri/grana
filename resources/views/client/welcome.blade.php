@extends('layouts.theme')
@section('title', 'گرانا بتن')

@section('content')
<section id="slider">
    <div class="container-fluid">
        <div class="row">
            <div id="carousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel" data-slide-to="1"></li>
                </ol><!-- /.carousel-indicators -->

                <!-- Wrapper for slides -->
                <div class="carousel-inner carousel-inner-custom" role="listbox">
                    <div class="item active">
                        <img src="img/slider-2.jpg" alt="...">
                        <div class="carousel-caption">
                            <h3>لورم ایپسون متن ساختگی است که طراحان و عکاسان از آن استفاده میکنند</h3>
                            <p>پروژه 1</p>
                        </div><!-- /.carousel-caption -->
                    </div><!-- /.item -->
                    <div class="item">
                        <img src="img/slider-2.jpg" alt="...">
                        <div class="carousel-caption">
                            <h3>لورم ایپسون متن ساختگی است که طراحان و عکاسان از آن استفاده میکنند</h3>
                            <p>پروژه 2</p>
                        </div><!-- /.carousel-caption -->
                    </div><!-- /.item -->
                </div><!-- /.carousel-inner -->

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div><!-- /#carousel -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section><!-- /section#slider -->
<div class="call-action">
    ما متخصص در این زمینه هستیم، بهترین راه حل های ساختمان
    &nbsp;
    <a href="#" class="btn btn-theme">بیشتر بدانید</a>
</div><!-- /.call-action -->
<section id="service-content">
    <div class="container">
        <div class="row">
            <div class="content-s-box col-xs-12 col-sm-4">
                <img class="img-responsive center-block" src="img/c-1.jpg" alt="image">
                <h4>طراحی مدرن</h4>
                <p class="text-justify">
                    لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. لورم ایپسوم یا طرح‌نمابه متنی آزمایشی و بی‌معنی در صنعت چاپ
                </p>
                <a href="#" class="text-more">ادامه مطلب</a>
            </div><!-- /.col-x-x -->
            <div class="content-s-box col-xs-12 col-sm-4">
                <img class="img-responsive center-block" src="img/c-2.jpg" alt="image">
                <h4>مدیریت ساخت و ساز</h4>
                <p class="text-justify">
                    لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. لورم ایپسوم یا طرح‌نمابه متنی آزمایشی و بی‌معنی در صنعت چاپ
                </p>
                <a href="#" class="text-more">ادامه مطلب</a>
            </div><!-- /.col-x-x -->
            <div class="content-s-box col-xs-12 col-sm-4">
                <img class="img-responsive center-block" src="img/c-3.jpg" alt="image">
                <h4>پیمانکار عمومی</h4>
                <p class="text-justify">
                    لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. لورم ایپسوم یا طرح‌نمابه متنی آزمایشی و بی‌معنی در صنعت چاپ
                </p>
                <a href="#" class="text-more">ادامه مطلب</a>
            </div><!-- /.col-x-x -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /section# -->
<section id="projects">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <h3 class="text-head">پروژه های اخیر</h3>
                <div class="text-head-line"></div>
            </div><!-- /.col-x-x -->
            <div class="col-xs-12 col-md-9">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-tabs-project">
                    <li class="active"><a href="#home" data-toggle="tab">همه پروژه ها</a></li>
                    @if(isset($lastProject))
                        @php $i=1; @endphp
                        @foreach($lastProject as $project)
                            <li><a href="#project{{$i}}" data-toggle="tab">{{$project->name}}</a></li>
                            @php $i++; @endphp
                        @endforeach
                    @endif
                </ul>
            </div><!-- /.col-x-x -->
            <div class="col-xs-12">
                <!-- Tab panes -->
                <div class="tab-content row">
                    <div class="tab-pane fade in active" id="home">

                        @if(isset($lastProject))
                            @php $i=1; @endphp
                            @foreach($lastProject as $project)
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <div class="projects-box">
                                        @if(sizeof($project->photos)>0)
                                            @php $i=1; @endphp
                                            @foreach($project->photos as $key=>$value)
                                                @if($i==1)
                                                    <img class="img-responsive center-block" src="/image/projects/{{$value->path}}">
                                                @endif
                                                @php $i++; @endphp
                                            @endforeach
                                        @else
                                            <img src="/image/no-photo.png">
                                        @endif
                                        <div class="overlay">
                                            <a href="{{url('/project', $project->id)}}" class="overlay-text">{{$project->name}}</a>
                                        </div><!-- /.overlay -->
                                    </div><!-- /.projects-box -->
                                </div><!-- /.col-x-x -->
                            @endforeach
                        @endif


                    </div><!-- /.tab-pane -->

                    @if(isset($lastProject))
                        @php $i=1; @endphp
                        @foreach($lastProject as $project)
                            <div class="tab-pane fade" id="project{{$i}}">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <div class="projects-box">
                                        @if(sizeof($project->photos)>0)
                                            @php $i=1; @endphp
                                            @foreach($project->photos as $key=>$value)
                                                @if($i==1)
                                                    <img class="img-responsive center-block" src="/image/projects/{{$value->path}}">
                                                @endif
                                                @php $i++; @endphp
                                            @endforeach
                                        @else
                                            <img src="/image/no-photo.png">
                                        @endif
                                        <div class="overlay">
                                            <a href="{{url('/project', $project->id)}}" class="overlay-text">{{$project->name}}</a>
                                        </div><!-- /.overlay -->
                                    </div><!-- /.projects-box -->
                                </div><!-- /.col-x-x -->
                            </div><!-- /.tab-pane -->
                            @php $i++; @endphp
                        @endforeach
                    @endif
                </div>
            </div><!-- /.col-x-x -->
        </div><!-- /.Row -->
    </div><!-- /.container -->
</section><!-- /#projects -->
<section id="news">
    <div class="container">
        <div class="row news-title">
            <h3 class="text-head" style="color: #333">آخرین اخبار</h3>
            <div class="text-head-line"></div>
        </div><!-- /.Row -->
        <div class="row">
            @if(isset($lastNews))
                @foreach($lastNews as $news)
                    <div class="col-xs-12 col-md-4">
                        <div class="new image">
                            <a href="{{url('/news', $news->id)}}"><img class="img-responsive center-block" src="/image/news/{{ $news->photo}}" alt="image"></a>
                        </div>
                        <div class="news-box clearfix">
                            <a href="{{url('/news', $news->id)}}">
                                <h4 class="news-box-title">{{$news->name}}</h4><!-- /.news-box-title -->
                            </a>
                            <p class="news-box-desc">
                              {{ str_limit($news->description ,350 ,'...')}}
                            </p>
                            <div class="post-meta-line "></div><!-- /.post-meta-line -->
                            <div class="news-box-more"><a href="{{url('/news', $news->id)}}">ادامه مطلب</a></div>
                        </div><!-- /.news-box -->
                    </div><!-- /.col-x-x -->
                @endforeach
            @endif
        </div><!-- /.Row -->
    </div><!-- /.container -->
</section><!-- /#news -->
@endsection
