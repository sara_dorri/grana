@extends('layouts.theme')
@section('title' ,'404')
@section('content')

{{--                     {{ __('messages.PageNotFound') }}--}}
{{--                    @lang('messages.PageNotFound')--}}
{{--                    {{ session()->get('locale') }}--}}

    <section id="contact-page">
        <div class="container-fluid">
            <div class="row">
                <div class="sp-page-title">
                    <div class="container">
                        <h3 class="sp-page-title-text">موردی یافت نشد .</h3>
                    </div><!-- /.container -->
                </div><!-- /.sp-page-title -->
            </div><!-- /.Row -->
        </div><!-- /.container-fluid -->
        <div class="container">
            <div class="row">
                <div class="notFound-box">
                    <div class="notFound-box-error">404</div>
                    <div class="notFound-box-title">متاسفانه صفحه مورد نظر شما یافت نشد .</div>
                </div><!-- /.notFound-box -->
            </div><!-- /.Row -->
        </div><!-- /.container -->
    </section>


@endsection


