<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';

    protected $guarded = ['id'];

    public function photos(){
        return $this->hasMany('App\PhotoProject');
    }
}
