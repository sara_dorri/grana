<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoProject extends Model
{
    protected $table = 'project_image';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function project(){
        return $this->belongsTo('App\Project');
    }
}
