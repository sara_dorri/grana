<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoProduct extends Model
{
    protected $table = 'product_image';

    protected $guarded = ['id'];

    public $timestamps = false;

    public function product(){
        return $this->belongsTo('App\Product');
    }
}
