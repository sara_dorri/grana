<?php

namespace App\Providers;

use App\Category;
use App\News;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
        $roots = Category::roots()->get();



        view()->composer('layouts.theme', function ($view) use ($roots) {
            $lastNews = News::orderBy('created_at', 'desc')->limit(4)->get();
            $view->with(['roots'=> $roots ,'news' => $lastNews]);

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
