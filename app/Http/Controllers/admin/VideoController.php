<?php

namespace App\Http\Controllers\admin;

use App\Media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VideoController extends Controller
{
    /**
     * Display videos.
     *
     *
     * @return RedirectResponse|View
     */
    public function index()
    {
        $videos = Media::where('type' ,'video')->get();
        return view('admin.media.video.index', compact('videos'));
    }


    /**
     * Display the upload page for videos.
     *
     * @return View
     */

    public function upload()
    {
        return view('admin.media.video.upload');
    }

    /**
     * Upload a video.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'path' => 'required' ,
            'name' => 'required',
            'description' => 'required',
        ], [
            'path.required' => 'لطفا آدرس ویدئو مورد نظر خود را وارد نمایید.',
            'file.mimes' => 'فایل انتخاب شده نامعتبر است.',
            'name.required' => 'لطفا نام ویدئو موردنظر را وارد نمایید.',
            'description.required' => 'لطفا توضیحات ویدئو را وارد کنید.',
        ]);

        $input = $request->all();

        $media = New Media();
        $media->name = $request->name;
        $media->description = $request->description;
        $media->path = $input['path'];
        $media->mimeType = '';
        $media->type = 'video';

        if ($media->save()) {
            return redirect()->back()->with('alert_success', 'بارگذاری ویدئو با موفقیت انجام شد.');
        }
        return redirect()->back()->with('alert_error', 'بارگذاری ویدئو ناموفق بود.');

    }



    /**
     * Download a video.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    /*public function getDownload(Request $request)
    {
        $input = $request->all();

        $path = $input['path'];
        $mimeType = $input['mimeType'];

        $file_path = public_path() . '\media\video\\' . $path;
        if (file_exists($file_path)) {

            return response()->download($file_path, $path, [
                'Content-Type' => $mimeType,
                'Content-Length: ' . filesize($file_path)
            ]);
        } else {
            return redirect()->back()->with('alert_error', 'ویدئو مورد نظر یافت نشد!');
        }
    }*/

    /**
     * Remove the specified video.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $media = Media::find($id);
        if($media->delete()){
            return redirect()->back()->with('alert_error', 'ویدئو با موفقیت حذف شد.');;
        }
        return redirect()->back()->with('alert_error', 'حذف ویدئو ناموفق بود، دوباره سعی کنید.');
    }
}
