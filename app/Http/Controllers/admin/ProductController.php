<?php

namespace App\Http\Controllers\admin;

use App\PhotoProduct;
use App\Product;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('admin.products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::getNestedList('name', null, '---');
        return view('admin.products.create' ,compact('categories' ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string',
            'category_id' => 'required',
            'photo.*' => 'image',
            'description' => 'required',
        ], [
            'name.required' => 'نام محصول را وارد کنید.',
            'name.string' => 'نام محصول را بصورت صحیح وارد کنید.',
            'category_id.required' => 'دسته بندی محصول را انتخاب کنید.',
            'photo.*.image' => 'فایل انتخابی معتبر نمی باشد، یک تصویر انتخاب کنید!',
            'description.required' => 'لطفا توضیحات محصول را وارد کنید.',

        ]);


        $input = $request->all();

        $product = Product::where('name' ,$input['name'])->first();
        if(sizeof($product)>0){
            return redirect()->back()->with('alert_error','هم اکنون محصولی با این نام وجود دارد.');
        }
        $pro = New Product();
        $pro->name = $request->name;
        $pro->category_id = $request->category_id;
        $pro->description = $request->description;
        $pro->save();


        if ($pro->save()){
            if ($files = $request->file('photo')) {
                foreach ($files as $file) {
                    $photo = New PhotoProduct();
                    $name = time() . rand(1000, 9999) .$file->getClientOriginalName();
                    $file->move('image/products', $name);
                    $photo->path = $name;
                    $photo->product_id = $pro->id;
                    $photo ->save();
                }
            }
            return redirect('/admin/products');
        }
        return redirect()->back()->with('alert_error', 'مشکلی در ثبت محصول به وجود آمده، دوباره سعی کنید.');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('admin.products.show' ,compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::getNestedList('name', null, '---');
        return view('admin.products.edit' ,compact('product' ,'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'category_id' => 'required',
            'photo.*' => 'image',
            'description' => 'required',
        ], [
            'name.required' => 'نام محصول را وارد کنید.',
            'name.string' => 'نام محصول  را بصورت صحیح وارد کنید.',
            'category_id.required' => 'دسته بندی محصول را انتخاب کنید.',
            'photo.*.image' => 'فایل انتخابی معتبر نمی باشد، یک تصویر انتخاب کنید!',
            'description.required' => 'لطفا توضیحات محصول را وارد کنید.',

        ]);

        $product = Product::find($id);
        $product->name = $request->name;
        $product->category_id = $request->category_id;
        $product->description = $request->description;

        if ($files = $request->file('photo')) {
            foreach ($files as $file) {
                $photo = New PhotoProduct();
                $name = time() . rand(1000, 9999) .$file->getClientOriginalName();
                $file->move('image/products', $name);
                $photo->path = $name;
                $photo->product_id = $product->id;
                $photo->save();
            }
        }


        if ($product->save()) {
           return redirect('/admin/products');
        }
        return redirect()->back()->with('alert_error', 'مشکلی در ویرایش محصول به وجود آمده، دوباره سعی کنید.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if($product->photos){
            foreach($product->photos as $key=>$value){
                unlink(public_path() .'/image/products/'. $value->path);
            }
        }
        $product->delete();
        return redirect('/admin/products');
    }

    public function destroyPhoto($id)
    {
        $photo = PhotoProduct::find($id);
        unlink(public_path() .'/image/products/'. $photo->path);
        $photo->delete();
        return redirect()->back();
    }

}
