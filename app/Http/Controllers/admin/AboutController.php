<?php

namespace App\Http\Controllers\admin;

use App\About;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abouts = About::all();
        $about = $abouts->first();
        return view('admin.about.index' ,compact('about'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.about.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subject' => 'required',
            'photo' => 'image',
            'description' => 'required',
        ], [
            'subject.required' => ' لطفا عنوان را وارد کنید.',
            'photo.image' => 'لطفا یک تصویر انتخاب کنید !',
            'description.required' => 'لطفا متن درباره ما را وارد کنید.',

        ]);

        $input = $request->all();

        if ($file = $request->file('photo')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('image/about', $name);
            $input['photo'] = $name;
        }
        if ($about = About::create($input)) {
            return redirect('/admin/about');
        }
        return redirect()->back()->with('alert_error', 'مشکلی در ثبت به وجود آمده، دوباره سعی کنید.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about = About::find($id);
        return view('admin.about.edit' ,compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'subject' => 'required',
            'photo' => 'image',
            'description' => 'required',
        ], [
            'subject.required' => ' لطفا عنوان را وارد کنید.',
            'photo.image' => 'لطفا یک تصویر انتخاب کنید !',
            'description.required' => 'لطفا متن درباره ما را وارد کنید.',

        ]);


        $input = $request->all();
        $about = About::find($id);

        if ($file = $request->file('photo')) {
            if($about->photo){

                unlink(public_path() .'/image/about/'. $about->photo);
            }

            $name = time() . $file->getClientOriginalName();
            $file->move('image/about', $name);
            $input['photo'] = $name;
        }


        if ($about->update($input)) {
            return redirect('/admin/about');
        }
        return redirect()->back()->with('alert_error', 'مشکلی در ویرایش به وجود آمده، دوباره سعی کنید.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $about = About::find($id);
        if($about->photo){
            unlink(public_path() .'/image/about/'. $about->photo);
        }
        $about->delete();
        return redirect('/admin/about');
    }
}
