<?php

namespace App\Http\Controllers\admin;

use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newss = News::all();
        return view('admin.news.index' ,compact('newss'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'photo' => 'image',
            'description' => 'required',
        ], [
            'name.required' => ' لطفا عنوان را وارد کنید.',
            'photo.image' => 'لطفا یک تصویر انتخاب کنید !',
            'description.required' => 'لطفا توضیحات را وارد کنید.',

        ]);

        $input = $request->all();

        if ($file = $request->file('photo')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('image/news', $name);
            $input['photo'] = $name;
        }
        $news = New News();
        $news->name = $request->name;
        $news->photo = $input['photo'];
        $news->description = $request->description;

        if ($news->save()) {
            return redirect()->back()->with('alert_success', 'خبر با موفقیت ثبت شد.');
        }
        return redirect()->back()->with('alert_error', 'مشکلی در ثبت به وجود آمده، دوباره سعی کنید.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::find($id);
        return view('admin.news.show' ,compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        return view('admin.news.edit' ,compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'photo' => 'image',
            'description' => 'required',
        ], [
            'name.required' => ' لطفا عنوان را وارد کنید.',
            'photo.image' => 'لطفا یک تصویر انتخاب کنید !',
            'description.required' => 'لطفا توضیحات را وارد کنید.',

        ]);


        $input = $request->all();
        $news = News::find($id);

        if ($file = $request->file('photo')) {
            if($news->photo){
                unlink(public_path() .'/image/news/'. $news->photo);
            }

            $name = time() . $file->getClientOriginalName();
            $file->move('image/news', $name);
            $input['photo'] = $name;
        }


        if ($news->update($input)) {
            return redirect('/admin/news');
        }
        return redirect()->back()->with('alert_error', 'مشکلی در ویرایش به وجود آمده، دوباره سعی کنید.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);
        if($news->photo){
            unlink(public_path() .'/image/news/'. $news->photo);
        }
        $news->delete();
        return redirect('/admin/news');
    }
}
