<?php

namespace App\Http\Controllers\admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * show form for change admin's password.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function updateSecurityForm()
    {
        $user = Auth::user();
        return view('admin.UpdateSecurity' ,compact('user'));
    }

    /**
     * update admin's password.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function updateSecurity(Request $request, $id)
    {
        $user = User::find($id);
        $this->validate($request, [
            'oldPassword' => 'required',
            'newPassword' => 'required|min:6|confirmed'
        ], [
            'oldPassword.required' => 'رمز پیشین خود را وارد کنید.',
            'newPassword.required' => 'لطفا رمز عبور جدید را وارد کنید.',
            'newPassword.min' => 'رمز انتخابی شما باید حداقل شامل 6 کاراکتر باشد.',
            'newPassword.confirmed' => 'تکرار رمز مطابقت ندارد.',

        ]);

        if (Hash::check($request->oldPassword, $user->password)) {
            if ($user->update(['password' => Hash::make($request->newPassword)])) {
                return redirect()->back()->with('alert_success', ' رمز عبور با موفقیت تغییر یافت.');
            }
            return redirect()->back()->with('alert_error', 'تغییر رمز عبور نا موفق بود!');
        }
        return redirect()->back()->with('alert_error', ' رمز عبور شما اشتباه است.');
    }
}
