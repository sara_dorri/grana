<?php

namespace App\Http\Controllers\admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $cat = Category::getNestedList('name', null, '---');
        return view('admin.categories.index',compact('categories' ,'cat'));
    }

    /**
     * Show the form for creating a new category.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $categories = Category::getNestedList('name', null, '---');
        return view('admin.categories.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $this->validate($request, [
            'name' => 'required',
            'photo' => 'image|max:300|mimes:jpeg,jpg,png',
            'description' => 'required|string',
        ], [
            'name.required' => 'شما باید یک نام برای دسته خود وارد کنید!',
            'photo.image' => 'فایل انتخابی معتبر نمی باشد، یک تصویر انتخاب کنید!',
            'photo.max' => 'حداکثر حجم تصویر 300 کیلوبایت می باشد!',
            'photo.mimes' => 'فرمت فایل انتخابی شما باید با پسوند های jpg،png باشد.',
            'description.required' => 'لطفا توضیحات دسته را وارد کنید.',

        ]);

        $cat = Category::where( 'name' ,$request->name )->first();
        if (sizeof($cat) > 0) {
            return redirect('/admin/category/create')->with('alert_error', 'هم اکنون یک دسته با این نام موجود می باشد.');
        }
        if ($file = $request->file('photo')) {
            $name = time() . $file->getClientOriginalName();
            $file-> move('image/category', $name);
            $input['photo'] = $name;
        }

        $node = Category::create($input);

        if ($request-> parent_id == '') {
            $node->makeRoot();
        } else {
            $node->makeChildOf($request->parent_id);
        }

        if ($node->getLevel() > 4) {
            $node->delete();
            return redirect('/admin/category/create')->with('alert_error', 'دسته بندی بیشتر از چهار سطح مجاز نمی باشد!');
        }
        return redirect('/admin/category/create')->with('alert_success', 'دسته بندی با موفقیت ایجاد شد.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $input = $request->all();
        $category = Category::find($id);


        if (!$request->name) {
            return redirect('/admin/category')->with('alert_error', 'در زمان ویرایش نام، باید یک نام جدید برای دسته خود وارد کنید!');
        }


        if ($file = $request->file('photo')) {
            unlink( public_path() .'/image/category/'. $category->photo );
            $name = time() . $file->getClientOriginalName();
            $file->move('image/category', $name);
            $input['photo'] = $name;

        }

        if ($category->update($input)) {
            return redirect('/admin/category')->with('alert_success', 'دسته بندی با موفقیت ویرایش شد.');
        }
        return redirect('/admin/category')->with('alert_error', 'ویرایش دسته موفقیت آمیز نبود. لطفا دوباره سعی کنید.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $node = Category::find($id);
        $childNodes = $node->getDescendants();
        foreach ($childNodes as $childNode) {
            $childNode->delete;
        }

        $node = Category::find($id);

        $products = $node->products;
        if (isset($products)) {
            foreach ($products as $products) {
                unlink(public_path() . $products->photo);
                $products->delete();
            }
        }
        $childNodes = $node->getDescendants();
        if (isset($childNodes)) {
            foreach ($childNodes as $childNode) {
                $productsOfChild = $childNode->products;
                foreach ($productsOfChild as $productOfChild) {
                    unlink(public_path() . $productOfChild->photo);
                    $productOfChild->delete();
                }
            }
        }

        if($node->photo){
            unlink(public_path() .'/image/category/'. $node->photo);
        }

        if ($node->delete()) {
            return redirect('/admin/category')->with('alert_success', 'دسته بندی با موفقیت حذف شد.');
        }
        return redirect('/admin/category')->with('alert_error', 'حذف دسته موفقیت آمیز نبود. لطفا دوباره سعی کنید.');
    }
}
