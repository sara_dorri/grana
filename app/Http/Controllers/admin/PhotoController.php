<?php

namespace App\Http\Controllers\admin;

use App\Media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PhotoController extends Controller
{
    /**
     * Display photos.
     *
     *
     * @return RedirectResponse|View
     */
    public function index()
    {
        $photos = Media::where('type' ,'photo')->paginate(15);
        return view('admin.media.photo.index', compact('photos'));
    }


    /**
     * Display the upload page for catalogs.
     *
     * @return View
     */

    public function upload()
    {
        return view('admin.media.photo.upload');
    }

    /**
     * Upload a catalog.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|file|max:1024|mimes:jpeg,jpg,png,gif,txt,pdf,doc,docx,ppt,pptx',
            'name' => 'required',
            'description' => 'required',
        ], [
            'file.required' => 'لطفا تصویر مورد نظر خود را بارگذاری نمایید.',
            'file.max' => 'حداکثر حجم مجاز برای تصویر 1 مگابایت می باشد.',
            'file.mimes' => 'فرمت های   jpg,png,gif ,txt , pdf , doc ,docx ,ppt, pptx مجاز هستند.',
            'name.required' => 'لطفا نام تصویر موردنظر را وارد نمایید.',
            'description.required' => 'لطفا توضیحات تصویر را به فرمت صحیح وارد کنید.',
        ]);

        $input = $request->all();


        if ($file = $request->file('file')) {
            $input['mimeType'] = strtolower($file->getClientOriginalExtension());
            $name = time() . $file->getClientOriginalName();
            $file->move('media/photo/', $name);
            $input['path'] = $name;
        }
        $media = New Media();
        $media->name = $request->name;
        $media->description = $request->description;
        $media->path = $input['path'];
        $media->mimeType = $input['mimeType'];
        $media->type = 'photo';

        if ($media->save()) {
            return redirect()->back()->with('alert_success', 'بارگذاری تصویر با موفقیت انجام شد.');
        }
        return redirect()->back()->with('alert_error', 'بارگذاری تصویر ناموفق بود.');

    }



    /**
     * Download a photo.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    /*public function getDownload(Request $request)
    {
        $input = $request->all();

        $path = $input['path'];
        $mimeType = $input['mimeType'];

        $file_path = public_path() . '\media\photo\\' . $path;
        if (file_exists($file_path)) {

            return response()->download($file_path, $path, [
                'Content-Type' => $mimeType,
                'Content-Length: ' . filesize($file_path)
            ]);
        } else {
            return redirect()->back()->with('alert_error', 'تصویر مورد نظر یافت نشد!');
        }
    }*/

    /**
     * Remove the specified photo.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $media = Media::find($id);
        if($media->delete()){
            unlink(public_path() .'\media\photo\\'. $media->path);
            return redirect()->back();

        }
        return redirect()->back()->with('alert_error', 'حذف تصویر ناموفق بود، دوباره سعی کنید.');
    }
}
