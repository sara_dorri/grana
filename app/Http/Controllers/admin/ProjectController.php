<?php

namespace App\Http\Controllers\admin;

use App\PhotoProject;
use App\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        return view('admin.projects.index',compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string',
            'photo.*' => 'image',
            'description' => 'required',
        ], [
            'name.required' => 'نام پروژه را وارد کنید.',
            'name.string' => 'نام پروژه را بصورت صحیح وارد کنید.',
            'photo.*.image' => 'فایل انتخابی معتبر نمی باشد، یک تصویر انتخاب کنید!',
            'description.required' => 'لطفا توضیحات پروژه را وارد کنید.',

        ]);


        $input = $request->all();

        $project = Project::where('name' ,$input['name'])->first();
        if(sizeof($project)>0){
            return redirect()->back()->with('alert_error','هم اکنون پروژه ای با این نام وجود دارد.');
        }

        $pro = New Project();
        $pro->name = $request->name;
        $pro->description = $request->description;
        $pro->save();


        if ($pro->save()){
            if ($files = $request->file('photo')) {
                foreach ($files as $file) {
                    $photo = New PhotoProject();
                    $name = time() . rand(1000, 9999) .$file->getClientOriginalName();
                    $file->move('image/projects', $name);
                    $photo->path = $name;
                    $photo->project_id = $pro->id;
                    $photo ->save();
                }
            }
            return redirect('/admin/projects');
        }
        return redirect()->back()->with('alert_error', 'مشکلی در ثبت پروژه به وجود آمده، دوباره سعی کنید.');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::find($id);
        return view('admin.projects.show' ,compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);
        return view('admin.projects.edit' ,compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'photo.*' => 'image',
            'description' => 'required',
        ], [
            'name.required' => 'نام پروژه را وارد کنید.',
            'name.string' => 'نام پروژه  را بصورت صحیح وارد کنید.',
            'photo.*.image' => 'فایل انتخابی معتبر نمی باشد، یک تصویر انتخاب کنید!',
            'description.required' => 'لطفا توضیحات پروژه را وارد کنید.',

        ]);

        $input = $request->all();
        $project = Project::find($id);
        $project->name = $request->name;
        $project->description =$request->description;


        if ($files = $request->file('photo')) {
            foreach ($files as $file) {
                $photo = New PhotoProject();
                $name = time() . rand(1000, 9999) .$file->getClientOriginalName();
                $file->move('image/projects', $name);
                $photo->path = $name;
                $photo->project_id = $project->id;
                $photo->save();
            }
        }


        if ($project->save()) {
            return redirect('/admin/projects');
        }
        return redirect()->back()->with('alert_error', 'مشکلی در ویرایش پروژه به وجود آمده، دوباره سعی کنید.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        if($project->photos){
            foreach($project->photos as $key=>$value){
                unlink(public_path() .'/image/projects/'. $value->path);
            }
        }
        $project->delete();
        return redirect('/admin/projects');
    }

    public function destroyPhoto($id)
    {
        $photo = PhotoProject::find($id);
        unlink(public_path() .'/image/projects/'. $photo->path);
        $photo->delete();
        return redirect()->back();
    }
}
