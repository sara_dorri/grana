<?php

namespace App\Http\Controllers\admin;

use App\Catalog;
use App\Media;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class CatalogController extends Controller
{
    /**
     * Display catalogs.
     *
     *
     * @return RedirectResponse|View
     */
    public function index()
    {
        $catalogs = Media::where('type' ,'catalog')->get();
        return view('admin.catalogs.index', compact('catalogs'));
    }


    /**
     * Display the upload page for catalogs.
     *
     * @return View
     */

     public function upload()
    {
        return view('admin.catalogs.upload');
    }

    /**
     * Upload a catalog.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|file|max:10240|mimes:jpeg,jpg,png,gif,txt,pdf,doc,docx,ppt,pptx',
            'name' => 'required',
            'description' => 'required',
        ], [
            'file.required' => 'لطفا فایل مورد نظر خود را بارگذاری نمایید.',
            'file.max' => 'حداکثر حجم مجاز برای فایل 10 مگابایت می باشد.',
            'file.mimes' => 'فرمت های   jpg,png,gif ,txt , pdf , doc ,docx ,ppt, pptx مجاز هستند.',
            'name.required' => 'لطفا نام فایل موردنظر را وارد نمایید.',
            'description.required' => 'لطفا توضیحات فایل را به فرمت صحیح وارد کنید.',
        ]);

        $input = $request->all();


        if ($file = $request->file('file')) {
            $input['mimeType'] = strtolower($file->getClientOriginalExtension());
            $name = time() . $file->getClientOriginalName();
            $file->move('catalog', $name);
            $input['path'] = $name;
        }
        $catalog = New Media();
        $catalog->name = $request->name;
        $catalog->description = $request->description;
        $catalog->path = $input['path'];
        $catalog->mimeType = $input['mimeType'];
        $catalog->type = 'catalog';

        if ($catalog->save()) {
            return redirect()->back()->with('alert_success', 'بارگذاری فایل با موفقیت انجام شد.');
        }
        return redirect()->back()->with('alert_error', 'بارگذاری فایل ناموفق بود.');

    }



    /**
     * Download a catalog.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function getDownload(Request $request)
    {
        $input = $request->all();

        $path = $input['path'];
        $mimeType = $input['mimeType'];

        $file_path = public_path() . '\catalog\\' . $path;
        if (file_exists($file_path)) {

            return response()->download($file_path, $path, [
                'Content-Type' => $mimeType,
                'Content-Length: ' . filesize($file_path)
            ]);
        } else {
            return redirect()->back()->with('alert_error', 'فایل مورد نظر یافت نشد!');
        }
    }

    /**
     * Remove the specified catalog.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $catalog = Media::find($id);
        if($catalog->delete()){
            unlink(public_path() .'\catalog\\'. $catalog->path);
            return redirect()->back();

        }
        return redirect()->back()->with('alert_error', 'حذف فایل ناموفق بود، دوباره سعی کنید.');
    }
}
