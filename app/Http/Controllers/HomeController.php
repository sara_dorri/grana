<?php

namespace App\Http\Controllers;

use App\News;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lastProject = Project::orderBy('created_at', 'desc')->limit(5)->get();
        $lastNews = News::orderBy('created_at', 'desc')->limit(3)->get();

        return view('client.welcome' ,compact('lastProject','lastNews'));
    }


}
