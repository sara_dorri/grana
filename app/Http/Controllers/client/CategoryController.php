<?php

namespace App\Http\Controllers\client;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function show($id)
    {
        $category = Category::find($id);
        $products = $category->products;
        return view('client.category' ,compact('products' ,'category'));
    }
}
