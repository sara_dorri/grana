<?php

namespace App\Http\Controllers\client;

use App\Media;
use App\News;
use App\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ClientController extends Controller
{
    public function about()
    {
        $about = DB::table('abouts')->first();
        return view('client.about' ,compact('about'));
    }

    public function contact()
    {
        return view('client.contact');
    }

    public function send(Request $request)
    {
        $input = $request->all();
        $sender = $input['name'];
        $content = $input['description'];
        $email = $input['email'];
        Mail::send('client.send',['sender' => $sender ,'email' => $email ,'content' => $content ],function ($message){
            $message
                ->to('saradorri_90@yahoo.com')
                ->from('saradorri_90@yahoo.com')
                ->subject('تست');
        });
        return redirect()->back()->with('alert-success','ایمیل شما با موفقیت ارسال شد');
    }


    public function photo()
    {
        $photo = Media::where('type' , 'photo')->get();
        return view('client.photo' ,compact('photo'));
    }


    public function video()
    {
        $videos = Media::where('type' , 'video')->get();
        return view('client.video',compact('videos'));
    }

    public function project($id)
    {
        $project = Project::find($id);
        $lastProjects = Project::orderBy('created_at', 'desc')->limit(5)->get();
        return view('client.project',compact('project' ,'lastProjects'));
    }

    public function news($id)
    {
        $news = News::find($id);
        $lastNews = News::orderBy('created_at', 'desc')->limit(5)->get();
        return view('client.news',compact('news' ,'lastNews'));
    }



}
