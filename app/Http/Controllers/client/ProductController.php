<?php

namespace App\Http\Controllers\client;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function show($id)
    {
        $lastProducts = Product::orderBy('created_at', 'desc')->limit(5)->get();
        $product = Product::find($id);
        return view('client.product' ,compact('product','lastProducts'));

    }
}
